import { Ok, Result } from "ts-results"
import Browser from "webextension-polyfill"
import type { CacheItemInterface } from "./interfaces/cache-item"
import { info, error } from "./utils"

/**
 * @summary Clear all entries from cache
 * @returns
 */
export const clear = async (): Promise<Result<void, Error>> => {
  try {
    await Browser.storage.local.remove("cache")

    info("Cache has been cleared")
    return Ok.EMPTY
  } catch (e: any) {
    return error(e)
  }
}

/**
 * @summary Retrieve a cache entry
 * @param key Key of the cache entry to retrieve
 * @returns
 */
export const get = async (key: string): Promise<Result<any, Error>> => {
  const loadResult = await load()

  if (loadResult.ok) {
    if (loadResult.val?.[key]) {
      if (isValid(loadResult.val[key])) {
        return new Ok(loadResult.val[key])
      } else {
        return invalidate(key)
      }
    }

    return Ok.EMPTY
  } else {
    return loadResult
  }
}

/**
 * @summary Try to retrieve an entry from the cache.
 *          If we don't find it, call the callback then save the result in cache.
 * @param key The key to try to retrieve
 * @param callback The callback to call.
 * @param ttl Time To Live, in seconds
 * @returns
 */
export const getOrSave = async (
  key: string,
  callback: () => Promise<Result<any, Error>>,
  ttl = 600
): Promise<Result<any, Error>> => {
  const getResult = await get(key)

  if (getResult.ok) {
    if (getResult.val) {
      info(`Retrieved item "${key}" from cache`)

      return new Ok(getResult.val.data)
    } else {
      const dataResult = await callback()

      if (dataResult.ok) {
        await save(key, dataResult.val, ttl)

        return new Ok(dataResult.val)
      } else {
        return dataResult
      }
    }
  } else {
    return getResult
  }
}

/**
 * @summary Retrieve the whole cache
 * @returns
 */
export const load = async (): Promise<Result<any, Error>> => {
  try {
    const cache = (await Browser.storage.local.get("cache")).cache
    return cache !== undefined ? new Ok(cache) : new Ok({})
  } catch (e: any) {
    return error(e)
  }
}

/**
 * @summary Save a new cache entry
 * @param key The key of the cache entry to save
 * @param data The data of the cache entry to save
 * @param ttl Time To Live, in seconds
 * @returns
 */
export const save = async (key: string, data: any, ttl = 600): Promise<Result<void, Error>> => {
  try {
    const loadResult = await load()

    if (loadResult.ok) {
      const cache = loadResult.val
      cache[key] = {
        data,
        timestamp: Date.now(),
        ttl,
      }

      await Browser.storage.local.set({ cache })

      info(`Cache entry "${key}" has been saved for ${ttl} seconds.`)

      return Ok.EMPTY
    } else {
      return loadResult
    }
  } catch (e: any) {
    return error(e)
  }
}

/**
 * @summary Check if a cache item is still valid
 * @param item CacheItem
 * @returns
 */
export const isValid = (item: CacheItemInterface): boolean => {
  return Date.now() <= item.timestamp + item.ttl * 1000
}

/**
 * @summary Force to invalidate a cache entry
 * @param key Key of the cache entry to invalidate
 * @returns
 */
export const invalidate = async (key: string): Promise<Result<void, Error>> => {
  try {
    const loadResult = await load()

    if (loadResult.ok) {
      delete loadResult.val[key]

      await Browser.storage.local.set({ cache: loadResult.val })

      info(`Cache entry "${key}" has been invalidated`)

      return Ok.EMPTY
    } else {
      return loadResult
    }
  } catch (e: any) {
    return error(e)
  }
}
