export class VideoGetTitleError extends Error {
  message = "Unable to retrieve the video title from the API"
}
