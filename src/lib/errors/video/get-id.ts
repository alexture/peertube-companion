export class VideoGetIdError extends Error {
  message = "Unable to retrieve the id of the video"
}
