export class VideoGetDetailsError extends Error {
  message = "Unable to retrieve video information from the API"
}
