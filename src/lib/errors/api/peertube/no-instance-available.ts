export class NoInstanceAvailableError extends Error {
  message =
    "There is no instance actually available for being redirected to. Ensure your connection is stable then try again later."
}
