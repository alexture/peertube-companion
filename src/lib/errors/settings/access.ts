export class SettingsAccessError extends Error {
  message = "An error occured while trying to access the settings storage"
}
