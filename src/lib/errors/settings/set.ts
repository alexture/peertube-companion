export class SettingsSetError extends Error {
  message = "An error occured while trying to save settings object in browser storage"
}
