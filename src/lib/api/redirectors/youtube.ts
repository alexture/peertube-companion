import { Ok, Result } from "ts-results"
import * as Cache from "../../cache"
import { VideoGetDetailsError } from "../../errors/video/get-details"
import type { InstanceInterface } from "../../interfaces/instance"
import * as Settings from "../../settings"
import { error, fetchApi } from "../../utils"

/**
 * @summary Retrieve video details from API
 * @param id The id of the Youtube video
 * @returns
 */
export const getVideoDetails = async (id: string): Promise<Result<any, Error>> => {
  const loadResult = await Settings.load()

  if (loadResult.ok) {
    const settings = loadResult.val

    const requestUrl = new URL(settings.api.youtube.baseUrl)
    requestUrl.searchParams.append(
      "url",
      settings.redirector.youtube.preferredInstance.url + "/watch?v=" + id
    )
    requestUrl.searchParams.append("format", "json")

    const getOrSaveResult = await Cache.getOrSave(
      `youtube-getVideoDetails-${encodeURIComponent(id)}`,
      async () => fetchApi(requestUrl.href),
      settings.cache.ttl.youtube.getVideoDetails
    )

    if (getOrSaveResult.ok) {
      if ("type" in getOrSaveResult.val) {
        return getOrSaveResult
      } else {
        return error(new VideoGetDetailsError())
      }
    } else {
      return getOrSaveResult
    }
  } else {
    return loadResult
  }
}

/**
 * @summary Extract the title from video details. Details are acquired through API
 * @param id
 * @returns
 */
export const getVideoTitle = async (id: string): Promise<Result<string, Error>> => {
  const detailsResult = await getVideoDetails(id)

  if (detailsResult.ok) {
    return new Ok(detailsResult.val.title)
  } else {
    return detailsResult
  }
}

/**
 * @summary Generate an Instance object for YouTube
 * @returns
 */
export const pickInstance = async (): Promise<Result<InstanceInterface, Error>> => {
  return Settings.get("redirector.youtube.preferredInstance")
}

/**
 * @summary Retrieve a list containing the only one instance of YouTube
 * @returns
 */
export const getInstancesFromIndex = async (): Promise<Result<InstanceInterface[], Error>> => {
  const pickInstanceResult = await pickInstance()

  if (pickInstanceResult.ok) {
    return new Ok([pickInstanceResult.val])
  } else {
    return pickInstanceResult
  }
}

/**
 * @summary Retrieve a list containing the only one instance of YouTube.
 *          This function exists only to keep same structure between all redirectors
 * @returns
 */
export const getInstancesPing = async (): Promise<Result<InstanceInterface[], Error>> => {
  return getInstancesFromIndex()
}
