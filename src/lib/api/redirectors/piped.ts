import { Ok, Result } from "ts-results"
import { error, fetchApi, ping, sortInstancesByPing } from "../../utils"
import * as Settings from "../../settings"
import * as Cache from "../../cache"
import { VideoGetDetailsError } from "../../errors/video/get-details"
import type { InstanceInterface } from "../../interfaces/instance"
import { SERVICE } from "../../enums/service"
import type { PipedInstanceInterface } from "../../interfaces/piped-instance"

/**
 * @summary Retrieve Piped instances from Piped index
 * @param filterResults Set to false if you don't want to have results filtered.
 *                      Actual filter is: non-tor instances, having API, 100% uptime today
 * @returns
 */
export const getInstancesFromIndex = async (
  filterResults = true
): Promise<Result<PipedInstanceInterface[], Error>> => {
  const loadSettingsResult = await Settings.load()

  if (loadSettingsResult.ok) {
    const settings = loadSettingsResult.val

    return Cache.getOrSave(
      `piped-getInstancesFromIndex-${encodeURIComponent(
        settings.api.piped.indexUrl
      )}-${filterResults}`,
      async () => {
        const apiResult = await fetchApi(`${settings.api.piped.indexUrl}`)

        return new Ok(
          apiResult.val
            .filter(
              (entry: any) =>
                entry.frontendUrl && entry.apiUrl && entry.apiUrl !== "https://pipedapi.kavin.rocks"
            )
            .map((entry: any) => {
              return {
                url: entry.frontendUrl,
                ping: 0,
                service: SERVICE.PIPED,
                apiUrl: entry.apiUrl,
              } as InstanceInterface
            })
        )
      },
      settings.cache.ttl.piped.getInstancesFromIndex
    )
  } else {
    return loadSettingsResult
  }
}

/**
 * @summary Retrieve video details from API
 * @param id The id of the Piped video
 * @returns
 */
export const getVideoDetails = async (id: string): Promise<Result<any, Error>> => {
  const loadResult = await Settings.load()

  if (loadResult.ok) {
    const settings = loadResult.val

    if (settings.redirector.piped.preferredInstance.apiUrl === "") {
      const pickResult = await pickInstance()

      if (pickResult.ok) {
        settings.redirector.piped.preferredInstance = pickResult.val
      } else {
        return pickResult
      }
    }

    const getOrSaveResult = await Cache.getOrSave(
      `piped-getVideoDetails-${encodeURIComponent(id)}`,
      async () => fetchApi(`${settings.redirector.piped.preferredInstance.apiUrl}/streams/${id}`),
      settings.cache.ttl.piped.getVideoDetails
    )

    if (getOrSaveResult.ok) {
      if ("error" in getOrSaveResult.val) {
        return error(new VideoGetDetailsError())
      } else {
        return Ok(getOrSaveResult.val)
      }
    } else {
      return getOrSaveResult
    }
  } else {
    return loadResult
  }
}

/**
 * @summary Extract the title from video details. Details are acquired through API
 * @param id
 * @returns
 */
export const getVideoTitle = async (id: string): Promise<Result<string, Error>> => {
  const videoDetailsResult = await getVideoDetails(id)

  if (videoDetailsResult.ok) {
    return new Ok(videoDetailsResult.val.title)
  } else {
    return videoDetailsResult
  }
}

/**
 * @summary Pick a Piped instance.
 *          By default, we try to pick the fastest instance.
 *          The picked instance is saved in storage.
 * @param random Set to true if instance should be picked randomly.
 *               This method tries to pick between fastest instances first
 * @returns
 */
export const pickInstance = async (random = false): Promise<Result<InstanceInterface, Error>> => {
  let instance: PipedInstanceInterface

  if (random) {
    const instancesResult = await getInstancesFromIndex()

    if (instancesResult.ok) {
      const filteredInstances = instancesResult.val.filter((i) => {
        return i.ping < 300
      })

      const instances = filteredInstances.length > 0 ? filteredInstances : instancesResult.val // If no instance is fast, pick between all of them

      instance = instances[Math.floor(Math.random() * instances.length)]
    } else {
      return instancesResult
    }
  } else {
    const getFastestResult = await getFastestInstance()

    if (getFastestResult.ok) {
      instance = getFastestResult.val
    } else {
      return getFastestResult
    }
  }

  const setResult = await Settings.set("redirector.piped.preferredInstance", instance)

  if (setResult.ok) {
    return new Ok(instance)
  } else {
    return setResult
  }
}

/**
 * @summary Retrieve the fastest Piped instance
 * @returns
 */
export const getFastestInstance = async (): Promise<Result<PipedInstanceInterface, Error>> => {
  const getInstancesResult = await getInstancesPing()

  if (getInstancesResult.ok) {
    return new Ok(getInstancesResult.val.sort(sortInstancesByPing)[0])
  } else {
    return getInstancesResult
  }
}

/**
 * @summary Retrieve ping of each Piped instance
 * @returns
 */
export const getInstancesPing = async (): Promise<Result<PipedInstanceInterface[], Error>> => {
  const getInstancesResult = await getInstancesFromIndex()

  if (getInstancesResult.ok) {
    return new Ok(
      (
        await Promise.all(
          getInstancesResult.val.map(async (instance: PipedInstanceInterface) => {
            const pingResult = await ping(new URL(instance.url + "/trending"), 1500, true)

            return {
              url: instance.url,
              ping: pingResult.ok ? pingResult.val : 0,
              service: SERVICE.PIPED,
              apiUrl: instance.apiUrl,
            }
          })
        )
      ).filter((instance: PipedInstanceInterface) => instance.ping > 0)
    )
  } else {
    return getInstancesResult
  }
}
