import * as Settings from "../../settings"
import * as Cache from "../../cache"
import { error, fetchApi, ping, sortInstancesByPing } from "../../utils"
import { Ok, Result } from "ts-results"
import { SERVICE } from "../../enums/service"
import type { InstanceInterface } from "../../interfaces/instance"
import { VideoGetDetailsError } from "../../errors/video/get-details"

/**
 * @summary Retrieve Invidious instances from Invidious index
 * @param filterResults Set to false if you don't want to have results filtered.
 *                      Actual filter is: non-tor instances, having API, 100% uptime today
 * @returns
 */
export const getInstancesFromIndex = async (
  filterResults = true
): Promise<Result<InstanceInterface[], Error>> => {
  const loadSettingsResult = await Settings.load()

  if (loadSettingsResult.ok) {
    const settings = loadSettingsResult.val

    return Cache.getOrSave(
      `invidious-getInstancesFromIndex-${encodeURIComponent(
        settings.api.invidious.indexUrl
      )}-${filterResults}`,
      async () => {
        const apiResult = await fetchApi(
          `${settings.api.invidious.indexUrl}/instances.json?pretty=1&sort_by=type,users`
        )

        if (apiResult.ok) {
          const jsonResponse = apiResult.val

          if (filterResults) {
            return new Ok(
              jsonResponse
                .filter((instance: any[]) => {
                  const baseCondition = instance[1].type !== "onion" && instance[1].api

                  return instance[1].monitor
                    ? baseCondition && instance[1].monitor.dailyRatios[0].ratio === "100.00"
                    : baseCondition
                })
                .map((entry: any) => {
                  return {
                    url: entry[1] ? entry[1].uri : entry.uri,
                    ping: 0,
                    service: SERVICE.INVIDIOUS,
                  } as InstanceInterface
                })
            )
          } else {
            return new Ok(
              jsonResponse.map((entry: any) => {
                return {
                  url: entry[1] !== undefined ? entry[1].uri : entry.uri,
                  ping: 0,
                  service: SERVICE.INVIDIOUS,
                } as InstanceInterface
              })
            )
          }
        } else {
          return apiResult
        }
      },
      settings.cache.ttl.invidious.getInstancesFromIndex
    )
  } else {
    return loadSettingsResult
  }
}

/**
 * @summary Retrieve video details from API
 * @param id The id of the Invidious video
 * @returns
 */
export const getVideoDetails = async (id: string): Promise<Result<any, Error>> => {
  const loadResult = await Settings.load()

  if (loadResult.ok) {
    const settings = loadResult.val

    if (settings.redirector.invidious.preferredInstance.url === "") {
      const pickResult = await pickInstance()

      if (pickResult.ok) {
        settings.redirector.invidious.preferredInstance = pickResult.val
      } else {
        return pickResult
      }
    }

    const getOrSaveResult = await Cache.getOrSave(
      `invidious-getVideoDetails-${encodeURIComponent(id)}`,
      async () =>
        fetchApi(`${settings.redirector.invidious.preferredInstance.url}/api/v1/videos/${id}`),
      settings.cache.ttl.invidious.getVideoDetails
    )

    if (getOrSaveResult.ok) {
      if ("type" in getOrSaveResult.val) {
        return Ok(getOrSaveResult.val)
      } else {
        return error(new VideoGetDetailsError())
      }
    } else {
      return getOrSaveResult
    }
  } else {
    return loadResult
  }
}

/**
 * @summary Extract the title from video details. Details are acquired through API
 * @param id
 * @returns
 */
export const getVideoTitle = async (id: string): Promise<Result<string, Error>> => {
  const videoDetailsResult = await getVideoDetails(id)

  if (videoDetailsResult.ok) {
    return new Ok(videoDetailsResult.val.title)
  } else {
    return videoDetailsResult
  }
}

/**
 * @summary Pick an Invidious instance.
 *          By default, we try to pick the fastest instance.
 *          The picked instance is saved in storage.
 * @param random Set to true if instance should be picked randomly.
 *               This method tries to pick between fastest instances first
 * @returns
 */
export const pickInstance = async (random = false): Promise<Result<InstanceInterface, Error>> => {
  let instance: InstanceInterface

  if (random) {
    const instancesResult = await getInstancesFromIndex()

    if (instancesResult.ok) {
      const filteredInstances = instancesResult.val.filter((i) => {
        return i.ping < 300
      })

      const instances = filteredInstances.length > 0 ? filteredInstances : instancesResult.val // If no instance is fast, pick between all of them

      instance = instances[Math.floor(Math.random() * instances.length)]
    } else {
      return instancesResult
    }
  } else {
    const getFastestResult = await getFastestInstance()

    if (getFastestResult.ok) {
      instance = getFastestResult.val
    } else {
      return getFastestResult
    }
  }

  const setResult = await Settings.set("redirector.invidious.preferredInstance", instance)

  if (setResult.ok) {
    return new Ok(instance)
  } else {
    return setResult
  }
}

/**
 * @summary Retrieve the fastest Invidious instance
 * @returns
 */
export const getFastestInstance = async (): Promise<Result<InstanceInterface, Error>> => {
  const getInstancesResult = await getInstancesPing()

  if (getInstancesResult.ok) {
    return new Ok(getInstancesResult.val.sort(sortInstancesByPing)[0])
  } else {
    return getInstancesResult
  }
}

/**
 * @summary Retrieve ping of each Invidious instance
 * @returns
 */
export const getInstancesPing = async (): Promise<Result<InstanceInterface[], Error>> => {
  const getInstancesResult = await getInstancesFromIndex()

  if (getInstancesResult.ok) {
    return new Ok(
      (
        await Promise.all(
          getInstancesResult.val.map(async (instance: any) => {
            const pingResult = await ping(new URL(instance.url + "/api/v1/stats"), 1500, true)

            return {
              url: instance.url,
              ping: pingResult.ok ? pingResult.val : 0,
              service: SERVICE.INVIDIOUS,
            }
          })
        )
      ).filter((instance: InstanceInterface) => instance.ping > 0)
    )
  } else {
    return getInstancesResult
  }
}
