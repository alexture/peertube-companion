import { Ok, type Result } from "ts-results"
import { REDIRECTOR } from "../enums/redirector"
import type { InstanceInterface } from "../interfaces/instance"
import * as Settings from "../settings"
import * as Invidious from "./redirectors/invidious"
import * as Youtube from "./redirectors/youtube"
import * as Piped from "./redirectors/piped"
import { VideoGetTitleError } from "../errors/video/get-title"
import { error, sendMessageToAllTabs, sendMessageToRuntime } from "../utils"
import { BROWSER_MESSAGE } from "../enums/browser-message"
import * as Cache from "../cache"

/**
 * @summary Get a list of Instance objects, depending on the preferred Redirector API
 * @returns
 */
export const getInstancesFromIndex = async (): Promise<Result<InstanceInterface[], Error>> => {
  const loadResult = await Settings.load()

  if (loadResult.ok) {
    const settings = loadResult.val
    let getInstancesFromIndexResult: Result<InstanceInterface[], Error>

    switch (settings.redirector.selected) {
      case REDIRECTOR.INVIDIOUS:
        getInstancesFromIndexResult = await Invidious.getInstancesFromIndex()
        break

      case REDIRECTOR.PIPED:
        getInstancesFromIndexResult = await Piped.getInstancesFromIndex()
        break

      case REDIRECTOR.YOUTUBE:
      default:
        getInstancesFromIndexResult = await Youtube.getInstancesFromIndex()
    }

    return getInstancesFromIndexResult
  } else {
    return loadResult
  }
}

/**
 * @summary Pick an instance, depending on the preferred Redirector
 * @returns
 */
export const pickInstance = async (): Promise<Result<InstanceInterface, Error>> => {
  try {
    const isPickingResult = await Cache.get("isPickingInstance")

    if (isPickingResult.ok) {
      if (!isPickingResult.val) {
        const setPickingCacheResult = await Cache.save("isPickingInstance", true)

        if (setPickingCacheResult.ok) {
          const loadResult = await Settings.load()

          if (loadResult.ok) {
            const settings = loadResult.val
            let pickInstanceResult: Result<InstanceInterface, Error>

            switch (settings.redirector.selected as REDIRECTOR) {
              case REDIRECTOR.INVIDIOUS:
                if (settings.redirector.invidious.preferredInstance.url !== "") {
                  pickInstanceResult = new Ok(settings.redirector.invidious.preferredInstance)
                } else {
                  pickInstanceResult = await Invidious.pickInstance()
                }
                break

              case REDIRECTOR.PIPED:
                if (settings.redirector.piped.preferredInstance.url !== "") {
                  pickInstanceResult = new Ok(settings.redirector.piped.preferredInstance)
                } else {
                  pickInstanceResult = await Piped.pickInstance()
                }
                break

              case REDIRECTOR.YOUTUBE:
                pickInstanceResult = await Youtube.pickInstance()
            }

            const invalidatePickingCache = await Cache.invalidate("isPickingInstance")

            if (invalidatePickingCache.err) {
              return invalidatePickingCache
            }

            if (pickInstanceResult.ok) {
              sendMessageToRuntime(BROWSER_MESSAGE.SELECTED_REDIRECTOR_INSTANCE_UPDATED)
              sendMessageToAllTabs(BROWSER_MESSAGE.SELECTED_REDIRECTOR_INSTANCE_UPDATED)
            }
            return pickInstanceResult
          } else {
            return loadResult
          }
        } else {
          return setPickingCacheResult
        }
      } else {
        return error(new Error("Already trying to pick an instance"))
      }
    } else {
      return isPickingResult
    }
  } catch (e) {
    return error(e)
  }
}

/**
 * @summary Get ping from all instances of the preferred Redirector
 * @returns
 */
export const getInstancesPing = async (): Promise<Result<InstanceInterface[], Error>> => {
  const selectedRedirectorResult = await Settings.get("redirector.selected")

  let instancesResult: Result<InstanceInterface[], Error>

  if (selectedRedirectorResult.ok) {
    switch (selectedRedirectorResult.val as REDIRECTOR) {
      case REDIRECTOR.INVIDIOUS:
        instancesResult = await Invidious.getInstancesPing()

        break

      case REDIRECTOR.PIPED:
        instancesResult = await Piped.getInstancesPing()

        break

      case REDIRECTOR.YOUTUBE:
        instancesResult = await Youtube.getInstancesPing()
        break
    }

    return instancesResult
  } else {
    return selectedRedirectorResult
  }
}

/**
 * @summary Save selected instance for the preferred Redirector
 * @param url The url of the instance to save
 * @returns
 */
export const savePreferredInstance = async (
  instance: InstanceInterface,
  redirector: REDIRECTOR
): Promise<Result<void, Error>> => {
  switch (redirector) {
    case REDIRECTOR.INVIDIOUS:
      return Settings.set("redirector.invidious.preferredInstance", instance)

    case REDIRECTOR.PIPED:
      return Settings.set("redirector.piped.preferredInstance", instance)

    case REDIRECTOR.YOUTUBE:
      return Ok.EMPTY
  }
}

/**
 * @summary Get the preferred instance of the preferred Redirector
 * @returns
 */
export const getPreferredInstance = async (
  redirector: REDIRECTOR
): Promise<Result<InstanceInterface, Error>> => {
  switch (redirector) {
    case REDIRECTOR.INVIDIOUS:
      return Settings.get("redirector.invidious.preferredInstance")

    case REDIRECTOR.PIPED:
      return Settings.get("redirector.piped.preferredInstance")

    case REDIRECTOR.YOUTUBE:
      return Settings.get("redirector.youtube.preferredInstance")
  }
}

/**
 * @summary Retrieve the video title according to the selected Redirector
 * @param id
 * @param redirector
 * @returns
 */
export const getVideoTitle = async (
  id: string,
  redirector: REDIRECTOR
): Promise<Result<string, Error>> => {
  let title = ""

  switch (redirector) {
    case REDIRECTOR.INVIDIOUS: {
      const getInvidiousTitleResult = await Invidious.getVideoTitle(id)

      if (getInvidiousTitleResult.ok) {
        title = getInvidiousTitleResult.val
      } else {
        return error(new VideoGetTitleError())
      }
      break
    }
    case REDIRECTOR.PIPED: {
      const getPipedTitleResult = await Piped.getVideoTitle(id)

      if (getPipedTitleResult.ok) {
        title = getPipedTitleResult.val
      } else {
        return error(new VideoGetTitleError())
      }
      break
    }
    case REDIRECTOR.YOUTUBE: {
      const getYouTubeTitleResult = await Youtube.getVideoTitle(id)
      if (getYouTubeTitleResult.ok) {
        title = getYouTubeTitleResult.val
      } else {
        return error(new VideoGetTitleError())
      }

      break
    }
  }

  return new Ok(title)
}
