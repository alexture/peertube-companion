import { Ok, Result } from "ts-results"
import * as Cache from "../cache"
import { SERVICE } from "../enums/service"
import { NoInstanceAvailableError } from "../errors/api/peertube/no-instance-available"
import type { InstanceInterface } from "../interfaces/instance"
import * as Settings from "../settings"
import { error, fetchApi, ping } from "../utils"
import type { VideoInterface } from "../interfaces/video"

/**
 * @summary Retrieve available instance from a PeerTube index
 * @returns
 */
export const getInstancesFromIndex = async (): Promise<Result<InstanceInterface[], Error>> => {
  const loadSettingsResult = await Settings.load()

  if (loadSettingsResult.ok) {
    const settings = loadSettingsResult.val

    return Cache.getOrSave(
      `peertube-getInstancesFromIndex-${encodeURIComponent(settings.api.peertube.indexUrl)}`,
      async () => {
        const fetchResult = await fetchApi(
          `${settings.api.peertube.indexUrl}/api/v1/instances/hosts?count=9999999`
        )

        if (fetchResult.ok) {
          return new Ok(
            fetchResult.val.data.map((entry: any) => {
              return {
                url: `https://${entry.host}`,
                service: SERVICE.PEERTUBE,
              } as InstanceInterface
            })
          )
        } else {
          return fetchResult
        }
      },
      settings.cache.ttl.peertube.getIndexedInstances
    )
  } else {
    return loadSettingsResult
  }
}

/**
 * @summary Search, using the API, all videos corresponding to a given title
 * @param title Title of the video to search for
 * @returns
 */
export const searchVideosByTitle = async (title: string): Promise<Result<any, Error>> => {
  const getApiUrlResult = await getApiUrl(true)

  if (getApiUrlResult.ok) {
    const settingsLoadResult = await Settings.load()

    if (settingsLoadResult.ok) {
      const settings = settingsLoadResult.val

      return Cache.getOrSave(
        `searchByTitle-${encodeURIComponent(title)}`,
        async () => {
          const fetchResult = await fetchApi(
            `${getApiUrlResult.val}/v1/search/videos?search=${encodeURIComponent(title)}`
          )

          if (fetchResult.ok) {
            return new Ok(
              fetchResult.val.data.map((video: any): VideoInterface => {
                return {
                  name: video.name,
                  url: video.url,
                  uuid: video.uuid,
                }
              })
            )
          } else {
            return fetchResult
          }
        },
        settings.cache.ttl.peertube.searchByTitle
      )
    } else {
      return settingsLoadResult
    }
  } else {
    return getApiUrlResult
  }
}

/**
 * @summary Retrieve the first available videos from the list
 * @param videos The list of PeerTube videos
 * @returns
 */
export const getFirstVideoAvailable = async (
  videos: VideoInterface[]
): Promise<Result<any, Error>> => {
  const getTTLResult = await Settings.get("cache.ttl.peertube.getFirstVideoAvailable")

  if (getTTLResult.ok) {
    for (const v of videos) {
      const pingResult = await ping(new URL(v.url), getTTLResult.val)
      if (pingResult.ok) {
        if (pingResult.val > 0) {
          return new Ok(v)
        }
      }
    }

    return error(new NoInstanceAvailableError())
  } else {
    return getTTLResult
  }
}

/**
 * @summary Generate the right API endpoint base, depending on isSearch parameter
 * @param isSearch Set to `true` if you want to get a Sepia Search API
 * @returns
 */
const getApiUrl = async (isSearch = false): Promise<Result<string, Error>> => {
  const settingsLoadResult = await Settings.load()

  if (settingsLoadResult.ok) {
    const settings = settingsLoadResult.val
    let url = settings.api.peertube.apiUrl
    if (!isSearch) {
      if (await isPreferredEnabled()) {
        url = settings.redirection.peertube.preferredInstance.url
      }
    } else {
      url = settings.api.peertube.searchUrl
    }

    return new Ok(`${url}/api`)
  } else {
    return settingsLoadResult
  }
}

/**
 * Update a PeerTube URL to handle lazy-loading.
 * This is necessary to be able to intercept URL in some cases
 * @param url
 * @returns
 */
export const handleLazyLoad = (url: URL) => {
  return url.pathname.includes("lazy-load-video;")
    ? new URL(decodeURIComponent(url.pathname.split("url=")[1]))
    : url
}

/**
 * @summary convert an url into an VideoInterface object
 * @param url
 * @returns
 */
export const urlToVideoObject = (url: URL): VideoInterface => {
  const pathnameSplit = url.pathname.split("/")

  return {
    url: url.origin + url.pathname,
    uuid: pathnameSplit[pathnameSplit.length - 1],
  } as VideoInterface
}

/**
 * @summary Convert a PeerTube URL to another one, according to Preferred Instance setting
 * @param id
 * @returns
 */
export const toPreferredInstance = async (id: string): Promise<Result<URL, Error>> => {
  const getPreferredResult = await Settings.get("redirection.peertube.preferredInstance.url")

  if (getPreferredResult.ok) {
    if (getPreferredResult.val !== "") {
      return new Ok(new URL(new URL(getPreferredResult.val).origin + "/videos/watch/" + id))
    } else {
      return error(new Error("Preferred PeerTube instance is empty"))
    }
  } else {
    return getPreferredResult
  }
}

/**
 * @summary Check if preferred instance has been choosen and if redirection to preferred instance is enabled
 * @returns
 */
export const isPreferredEnabled = async (): Promise<boolean> => {
  const loadSettingsResult = await Settings.load()

  if (loadSettingsResult.ok) {
    return (
      loadSettingsResult.val.redirection.rules.toPreferredPeertube.isEnabled &&
      loadSettingsResult.val.redirection.peertube.preferredInstance.url
    )
  } else {
    throw loadSettingsResult.val
  }
}
