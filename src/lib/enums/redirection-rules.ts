/**
 * @summary Rules for redirection
 */
export enum REDIRECTION_RULES {
  MATCHING_EXACT_TITLE,
  TO_PREFERRED_PEERTUBE_INSTANCE,
}
