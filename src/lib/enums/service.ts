/**
 * @summary List of different services with which we will interract
 */
export enum SERVICE {
  INVIDIOUS,
  PIPED,
  PEERTUBE,
  YOUTUBE,
  NO_SERVICE,
}
