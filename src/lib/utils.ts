import get from "lodash.get"

import { Err, Ok, Result } from "ts-results"
import { LOG_TYPE } from "./enums/log-type"
import type { InstanceInterface } from "./interfaces/instance"

import * as Settings from "./settings"
import { SettingsAccessError } from "./errors/settings/access"
import Browser from "webextension-polyfill"
import type { BROWSER_MESSAGE } from "./enums/browser-message"

/**
 * @summary Fetch data from the URL and return the JSON response
 * @param url - The URL to fetch from
 * @returns
 */
export const fetchApi = async (url: string): Promise<Result<any, Error>> => {
  try {
    info(`Fecthing URL ${url}`)

    const response = await fetch(url)

    return response
      ? new Ok(await response.json())
      : error(new Error("An error occurred while trying to fetch the API"))
  } catch (e: any) {
    return error(e)
  }
}

/**
 * @summary Ping an url
 * @param url
 * @param timeout How much time, in miliseconds, before aborting the ping
 * @param ignoreErrors
 * @returns
 */
export const ping = async (
  url: URL,
  timeout = 3000,
  ignoreErrors = false
): Promise<Result<number, Error>> => {
  info(`Ping "${url.href}"`)
  const TIME_START = new Date().getTime()
  const controller = new AbortController()
  const signal = controller.signal
  const headers = new Headers()

  headers.append("pragma", "no-cache")
  headers.append("cache-control", "no-cache")

  const fetchPromise = fetch(url.href, { headers, signal, method: "HEAD" })
  setTimeout(() => controller.abort(), timeout)

  return fetchPromise
    .then((request) => {
      if (!request.ok && !ignoreErrors) {
        return error(
          new Error(`HTTP ${request.status} error while trying to fetch "${request.url}"`)
        )
      }

      const t = new Date().getTime() - TIME_START

      info(`Pong "${url.href}": ${t}`)

      return new Ok(t)
    })
    .catch((e) => {
      return error(e)
    })
}

/**
 * @summary Log an error in the console and return an Err() object to handle it
 * @param errorObject Object extending Error interface
 * @returns
 */
export const error = (errorObject: Error): Err<Error> => {
  log(errorObject.message, LOG_TYPE.ERROR)
  return new Err(errorObject)
}

/**
 * @summary Display an informative message in the console
 * @param message
 */
export const info = (message: string): void => {
  log(message, LOG_TYPE.INFO)
}

/**
 * @summary Log a message in the console
 * @param message message to show in console
 * @param type type of message to log, it will applies the correct console method according to
 * this parameter
 */
export const log = (message: any, type?: LOG_TYPE): void => {
  const preMessage = "[PeerTube Companion] "
  switch (type) {
    case LOG_TYPE.ERROR:
      console.error(preMessage + message)

      break
    case LOG_TYPE.INFO:
      console.info(preMessage + message)
      break
    case LOG_TYPE.WARNING:
      console.warn(preMessage + message)
      break
    default:
      console.log(preMessage + message)
  }
}

/**
 * @summary Display a warning message in the console
 * @param message
 */
export const warn = (message: string): void => {
  log(message, LOG_TYPE.WARNING)
}

/**
 * @summary Sort two instances by their ping
 * @param instanceA
 * @param instanceB
 * @returns
 */
export const sortInstancesByPing = (instanceA: InstanceInterface, instanceB: InstanceInterface) => {
  if (instanceA.ping > instanceB.ping) return 1
  if (instanceA.ping < instanceB.ping) return -1
  return 0
}

/**
 * @summary Check if the app is enabled. If a settingKey is provided, it will check its isEnabled property
 * @param settingKey A string representing a specific key in config
 * @returns false if the app is disabled or if there is an error
 */
export const isEnabled = async (settingKey?: string): Promise<boolean> => {
  try {
    const settingsResult = await Settings.load()

    if (settingsResult.ok) {
      const settings = settingsResult.val

      if (settingKey) {
        return get(settings, settingKey)?.isEnabled || false
      }

      return settings.app.isEnabled
    } else {
      error(new SettingsAccessError())
      return false
    }
  } catch (e: any) {
    error(e)

    return false
  }
}

/**
 * @summary Send a message to all Content-scripts listeners
 * @param message
 * @param options
 * @returns
 */
export const sendMessageToAllTabs = async (
  message: BROWSER_MESSAGE,
  options?: Browser.Tabs.SendMessageOptionsType
): Promise<Result<void, Error>> => {
  try {
    const tabs = await Browser.tabs.query({})

    for (const tab of tabs) {
      if (tab.id && tab.url === Browser.runtime.getManifest().options_ui?.page) {
        Browser.tabs.sendMessage(tab.id, message, options)
      }
    }

    return Ok.EMPTY
  } catch (e: any) {
    return error(e)
  }
}

/**
 * @summary Send a message to Runtime. Ignore errors saying there is no established connection
 * @param message
 * @returns
 */
export const sendMessageToRuntime = async (
  message: BROWSER_MESSAGE
): Promise<Result<void, Error>> => {
  try {
    await Browser.runtime.sendMessage(message)

    return Ok.EMPTY
  } catch (e: any) {
    if (e.message !== "Could not establish connection. Receiving end does not exist.") {
      return error(e)
    } else {
      return Ok.EMPTY
    }
  }
}

/**
 * @summary Check if a string follows the UUID format
 * @param string
 * @returns
 */
export const isUuid = (string: string): boolean => {
  return /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(string)
}

/**
 * @summary Check if a string follows the shortUUID format (PT has shortUUId of 22 characters long)
 * @param string
 * @returns
 */
export const isShortUuid = (string: string): boolean => {
  return /^[0-9a-z]{22}$/i.test(string)
}

/**
 * @summary Check if a string follows the shortUUID format (PT has shortUUId of 22 characters long)
 * @param string
 * @returns
 */
export const isYoutubeId = (string: string): boolean => {
  return /^[0-9a-z_-]{11}$/i.test(string)
}
