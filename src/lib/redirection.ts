import Browser from "webextension-polyfill"
import { Ok, Result } from "ts-results"
import { error, info, isShortUuid, isUuid, isYoutubeId, warn } from "./utils"
import * as Cache from "./cache"
import * as Settings from "./settings"
import { SERVICE } from "./enums/service"
import * as Invidious from "./api/redirectors/invidious"
import * as Peertube from "./api/peertube"
import * as Piped from "./api/redirectors/piped"
import type { HostInterface } from "./interfaces/host"
import * as Redirector from "./api/redirector"
import type { VideoInterface } from "./interfaces/video"
import { VideoGetIdError } from "./errors/video/get-id"

/**
 * Disable redirection handling
 * @returns
 */
export const disableRedirection = async (): Promise<Result<void, Error>> => {
  try {
    await Browser.webRequest.onBeforeRequest.removeListener(redirectionCallback)

    info("Redirection has been disabled")
    return Ok.EMPTY
  } catch (e) {
    return error(e)
  }
}

/**
 * @summary Enable redirection handling
 * @returns
 */
export const enableRedirection = async (): Promise<Result<void, Error>> => {
  try {
    const pickInstanceResult = await Redirector.pickInstance()

    if (pickInstanceResult.ok) {
      const generateHostsToRedirectResult = await generateHostsToRedirect()

      if (generateHostsToRedirectResult.ok) {
        const hosts = generateHostsToRedirectResult.val

        Browser.webRequest.onBeforeRequest.addListener(
          redirectionCallback,
          {
            urls: hosts.map((host: HostInterface) => host.url),
            types: ["main_frame"],
          },
          ["blocking"]
        )

        info("Redirection has been enabled")
        return Ok.EMPTY
      } else {
        return generateHostsToRedirectResult
      }
    } else {
      return pickInstanceResult
    }
  } catch (e: any) {
    return error(e)
  }
}

/**
 * @summary If app is enabled, reload it
 * @returns
 */
export const reloadRedirection = async (): Promise<Result<void, Error>> => {
  const settingsResult = await Settings.load()

  if (settingsResult.ok) {
    if (settingsResult.val.app.isEnabled) {
      info("Reloading the app")

      await disableRedirection()
      await enableRedirection()
    }
    return Ok.EMPTY
  } else {
    return settingsResult
  }
}

/**
 * @summary This is the callback called from the WebRequest Listener
 * @param request This parameter is provided from WebRequest listener
 * @returns
 */
const redirectionCallback = async (
  request: Browser.WebRequest.OnBeforeRequestDetailsType
): Promise<any> => {
  try {
    const requestUrl = new URL(request.url)

    const loadSettingsResult = await Settings.load()
    if (loadSettingsResult.err) {
      throw loadSettingsResult.val
    }

    const settings = loadSettingsResult.val

    // Do not redirect if coming from the preferred url. This is to avoid redirections loop
    const preferredPeerTubeInstance = settings.redirection.peertube.preferredInstance.url

    if (await Peertube.isPreferredEnabled()) {
      if (request.originUrl) {
        if (new URL(request.originUrl).origin === new URL(preferredPeerTubeInstance).origin) {
          info("Exiting preferred PeerTube instance. Skipping redirection")
          return {}
        }
      } else {
        if (requestUrl.origin === new URL(preferredPeerTubeInstance).origin) {
          info("Preferred PeerTube instance detected. Skipping redirection.")
          return {}
        }
      }
    }

    const cacheGetResult = await Cache.get("lastRedirectionUrl")

    if (cacheGetResult.ok) {
      // Do not redirect an URL which just has been redirected by the extension
      if (!cacheGetResult.val || cacheGetResult.val.data !== request.url) {
        if (await isAllowedUrl(requestUrl)) {
          info("Handling redirection of url " + request.url)
          const time = new Date().getTime()

          const targetUrlResult = await generateTargetUrl(requestUrl)

          if (targetUrlResult.ok && targetUrlResult.val) {
            info(`Redirection handled in ${new Date().getTime() - time} ms`)

            Cache.save(
              "lastRedirectionUrl",
              targetUrlResult.val.url,
              settings.cache.ttl.redirection.last
            )

            return {
              redirectUrl: targetUrlResult.val.url,
            }
          }

          info("Skipping redirection.")
        }
      }
    }

    return {} // Do not redirect
  } catch (e) {
    error(e)
    return {}
  }
}

/**
 * @summary Generate a list of available hosts to target for redirection
 * @returns A list of objects following the custom Host interface
 */
const generateHostsToRedirect = async (): Promise<Result<HostInterface[], Error>> => {
  const settingsResult = await Settings.load()

  if (settingsResult.ok) {
    const settings = settingsResult.val

    return Cache.getOrSave(
      `generateHostsToRedirect-${settings.redirection.invidious.isEnabled}-${settings.redirection.peertube.isEnabled}-${settings.redirection.piped.isEnabled}-${settings.redirection.youtube.isEnabled}`,
      async () => {
        const hosts: HostInterface[] = []

        if (settings.redirection.invidious.isEnabled) {
          const invidiousIndexedInstancesResult = await Invidious.getInstancesFromIndex(false)

          if (invidiousIndexedInstancesResult.ok) {
            invidiousIndexedInstancesResult.val.map((instance) => {
              hosts.push({
                url: instance.url + "/*",
                service: instance.service,
              })
            })
          } else {
            return invidiousIndexedInstancesResult
          }
        }

        if (settings.redirection.peertube.isEnabled) {
          const peertubeIndexedInstancesResult = await Peertube.getInstancesFromIndex()

          if (peertubeIndexedInstancesResult.ok) {
            peertubeIndexedInstancesResult.val.map((instance) => {
              hosts.push(
                {
                  url: instance.url + "/w/*",
                  service: instance.service,
                },
                {
                  url: instance.url + "/videos/watch/*",
                  service: instance.service,
                }
              )
            })
          } else {
            return peertubeIndexedInstancesResult
          }
        }

        if (settings.redirection.piped.isEnabled) {
          const pipedIndexedInstancesResult = await Piped.getInstancesFromIndex(false)

          if (pipedIndexedInstancesResult.ok) {
            pipedIndexedInstancesResult.val.map((instance) => {
              hosts.push({
                url: instance.url + "/*",
                service: instance.service,
              })
            })
          } else {
            return pipedIndexedInstancesResult
          }
        }

        if (settings.redirection.youtube.isEnabled) {
          settings.api.youtube.hostsToRedirect.map((host: string) =>
            hosts.push({
              url: `*://${host}/*`,
              service: SERVICE.YOUTUBE,
            } as HostInterface)
          )
        }

        return new Ok(hosts)
      },
      settings.cache.ttl.redirection.generateHostsToRedirect
    )
  } else {
    return settingsResult
  }
}

/**
 * @summary Generate the appropriate url to redirect to according to settings
 * @param url The URL from which we want to be redirected from
 * @returns
 */
const generateTargetUrl = async (url: URL): Promise<Result<any, Error>> => {
  const settingsLoadResult = await Settings.load()

  if (settingsLoadResult.ok) {
    const settings = settingsLoadResult.val

    let videos: VideoInterface[] = []

    const urlServiceResult = await getUrlService(url)
    const isPreferredPeertubeEnabled = await Peertube.isPreferredEnabled()

    if (urlServiceResult.ok) {
      const matchExactRule = settings.redirection.rules.matchExactTitle

      // Match Exact Title Rule
      if (matchExactRule.isEnabled && matchExactRule.scopes.includes(urlServiceResult.val)) {
        const idResult = await extractId(url)

        if (idResult.ok) {
          const titleResult = await Redirector.getVideoTitle(
            idResult.val,
            settings.redirector.selected
          )

          if (titleResult.ok) {
            const title = titleResult.val

            const searchVideosByTitleResult = await Peertube.searchVideosByTitle(title)

            if (searchVideosByTitleResult.ok) {
              videos = searchVideosByTitleResult.val.filter((video: any) => {
                return video.name.toLowerCase() === title.toLowerCase()
              })
            } else {
              return searchVideosByTitleResult
            }
          } else {
            return titleResult
          }
        } else {
          return idResult
        }
      } else {
        // Preferred Peertube Instance Rule
        if (
          isPreferredPeertubeEnabled &&
          settings.redirection.rules.toPreferredPeertube.scopes.includes(urlServiceResult.val)
        ) {
          const idResult = await extractId(url)

          if (idResult.ok) {
            const preferredUrlResult = await Peertube.toPreferredInstance(idResult.val)

            if (preferredUrlResult.ok) {
              videos = [Peertube.urlToVideoObject(preferredUrlResult.val)]
            } else {
              return preferredUrlResult
            }
          } else {
            return idResult
          }
        }
      }
    } else {
      return urlServiceResult
    }

    if (videos.length > 0) {
      if (isPreferredPeertubeEnabled) {
        const preferredVideos: VideoInterface[] = []

        for (const video of videos) {
          let url = video.url
          if (isPreferredPeertubeEnabled) {
            url =
              settings.redirection.peertube.preferredInstance.url + new URL(video.url).pathname ||
              url
          }

          preferredVideos.push({
            url,
            uuid: video.uuid,
            name: video.name,
          })
        }

        const firstAvailableResult = await Peertube.getFirstVideoAvailable(preferredVideos)

        if (firstAvailableResult.ok) {
          return firstAvailableResult
        } else {
          warn("No video available on preferred instance.")
        }
      }
      return await Peertube.getFirstVideoAvailable(videos)
    } else {
      return Ok.EMPTY
    }
  } else {
    return settingsLoadResult
  }
}

/**
 * @summary Retrieve the kind of service of the URL
 * @param url
 * @returns
 */
const getUrlService = async (url: URL): Promise<Result<SERVICE, Error>> => {
  const ttlResult = await Settings.get("cache.ttl.redirection.getUrlService")

  if (ttlResult.ok) {
    return Cache.getOrSave(
      `getUrlService-${encodeURIComponent(url.host)}`,
      async (): Promise<Result<SERVICE, Error>> => {
        const hosts = await generateHostsToRedirect()

        if (hosts.ok) {
          for (const host of hosts) {
            if (host.url.includes(url.host)) {
              return new Ok(host.service)
            }
          }

          return new Ok(SERVICE.NO_SERVICE)
        } else {
          return hosts
        }
      },
      ttlResult.val
    )
  } else {
    return ttlResult
  }
}

/**
 * @summary Check if the given URL is allowed to be redirected.
 *          It basically just try to extract the ID and if not possible, returns false.
 * @param url
 * @returns
 */
const isAllowedUrl = async (url: URL): Promise<boolean> => {
  return (await extractId(url)).ok
}

/**
 * @summary Extract the Id of the video from the URL
 * @param url
 * @returns
 */
const extractId = async (url: URL): Promise<Result<string, Error>> => {
  const settingsResult = await Settings.load()

  if (settingsResult.ok) {
    return Cache.getOrSave(
      `extractId-${encodeURIComponent(url.href)}`,
      async () => {
        const getUrlServiceResult = await getUrlService(url)

        if (getUrlServiceResult.ok) {
          if (getUrlServiceResult.val !== SERVICE.NO_SERVICE) {
            switch (getUrlServiceResult.val) {
              case SERVICE.INVIDIOUS:
                {
                  for (const allowedPath of settingsResult.val.redirection.invidious.allowedPaths) {
                    if (allowedPath.path !== "/") {
                      if (url.pathname.startsWith(allowedPath.path)) {
                        const id = url.searchParams.get(allowedPath.parameter)

                        if (id && isYoutubeId(id)) {
                          return new Ok(id)
                        }
                      }
                    } else {
                      // If path is same length as youtubeId
                      const id = url.pathname.substring(1)

                      if (id.length === 11 && isYoutubeId(id)) {
                        return new Ok(id)
                      }
                    }
                  }
                }
                break

              case SERVICE.PIPED:
                {
                  for (const allowedPath of settingsResult.val.redirection.piped.allowedPaths) {
                    if (allowedPath.path !== "/") {
                      if (url.pathname.startsWith(allowedPath.path)) {
                        const id = url.searchParams.get(allowedPath.parameter)

                        if (id && isYoutubeId(id)) {
                          return new Ok(id)
                        }
                      }
                    } else {
                      // If path is same length as youtubeId
                      const id = url.pathname.substring(1)

                      if (id.length === 11 && isYoutubeId(id)) {
                        return new Ok(id)
                      }
                    }
                  }
                }
                break

              case SERVICE.PEERTUBE:
                {
                  for (const allowedPath of settingsResult.val.redirection.peertube.allowedPaths) {
                    if (url.pathname.startsWith(allowedPath.path)) {
                      const id = url.pathname.split("/").pop()

                      if (id && (isShortUuid(id) || isUuid(id))) {
                        return new Ok(id)
                      }
                    }
                  }
                }
                break

              case SERVICE.YOUTUBE: {
                for (const allowedPath of settingsResult.val.redirection.youtube.allowedPaths) {
                  if (allowedPath.path !== "/") {
                    if (url.pathname.startsWith(allowedPath.path)) {
                      const id = url.searchParams.get(allowedPath.parameter)

                      if (id && isYoutubeId(id)) {
                        return new Ok(id)
                      }
                    }
                  }
                }
              }
            }

            return error(new VideoGetIdError())
          } else {
            return error(new Error("Unrecognized service."))
          }
        } else {
          return getUrlServiceResult
        }
      },
      settingsResult.val.cache.ttl.redirection.extractId
    )
  } else {
    return settingsResult
  }
}
