export interface AllowedPathInterface {
  path: string
  parameter?: string
}
