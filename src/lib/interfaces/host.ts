import type { SERVICE } from "../enums/service"

export interface HostInterface {
  url: string
  service: SERVICE
}
