import type { InstanceInterface } from "./instance"

export interface PipedInstanceInterface extends InstanceInterface {
  apiUrl: string
}
