import { createApp } from "vue"
import App from "./components/Options.vue"
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"

createApp(App).component("font-awesome-icon", FontAwesomeIcon).mount("#app")
