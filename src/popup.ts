import { createApp } from "vue"
import App from "./components/Popup.vue"
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"

createApp(App).component("font-awesome-icon", FontAwesomeIcon).mount("#app")
