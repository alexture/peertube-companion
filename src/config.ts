import { SERVICE } from "./lib/enums/service"
import { REDIRECTOR } from "./lib/enums/redirector"
import type { InstanceInterface } from "./lib/interfaces/instance"
import type { PipedInstanceInterface } from "./lib/interfaces/piped-instance"
import type { AllowedPathInterface } from "./lib/interfaces/allowed-path"

export const config = {
  api: {
    invidious: {
      isEnabled: true,
      indexUrl: "https://api.invidious.io",
    },
    peertube: {
      indexUrl: "https://instances.joinpeertube.org",
      searchUrl: "https://sepiasearch.org",
    },
    piped: {
      isEnabled: true,
      indexUrl: "https://piped-index.dreads-unlock.fr",
    },
    youtube: {
      isEnabled: true,
      baseUrl: "https://www.youtube.com/oembed",
      hostsToRedirect: [
        "m.youtube.com",
        "youtube.com",
        "img.youtube.com",
        "www.youtube.com",
        "youtube-nocookie.com",
        "www.youtube-nocookie.com",
        "youtu.be",
        "s.ytimg.com",
      ],
    },
  },
  app: {
    isEnabled: true,
  },
  cache: {
    ttl: {
      invidious: {
        pickInstance: 10 * 60, // 10 minutes
        getInstancesFromIndex: 1 * 3600, // 1 hour
        getVideoDetails: 1 * 24 * 3600, // 1 day
      },
      peertube: {
        getIndexConfig: 1 * 3600,
        getInstancesFromIndex: (1 / 2) * 24 * 3600, // Every 12 hours
        getInstanceConfig: (1 / 4) * 24 * 3600, // Every 6 hours
        getFirstVideoAvailable: 1500, // in miliseconds
        getVideosDetails: 1 * 24 * 3600,
        searchByTitle: (1 / 2) * 3600, // 1/2 hour
      },
      piped: {
        pickInstance: 10 * 60, // 10 minutes
        getInstancesFromIndex: 1 * 3600, // 1 hour
        getVideoDetails: 1 * 24 * 3600, // 1 day
      },
      redirection: {
        callback: 1 * 24 * 3600, // 1 day
        last: 5,
        getUrlService: 1 * 24 * 3600,
        generateHostsToRedirect: 1 * 24 * 3600,
        extractId: 365 * 24 * 3600, // 1 year
      },
      youtube: {
        getVideoDetails: 1 * 24 * 3600, // 1 day
      },
    },
  },
  redirector: {
    selected: REDIRECTOR.YOUTUBE,
    invidious: {
      enabled: true,
      preferredInstance: {
        url: "",
        ping: 0,
        service: SERVICE.INVIDIOUS,
      } as InstanceInterface,
    },
    piped: {
      enabled: true,
      preferredInstance: {
        url: "",
        ping: 0,
        service: SERVICE.PIPED,
        apiUrl: "",
      } as PipedInstanceInterface,
    },
    youtube: {
      enabled: true,
      preferredInstance: {
        url: "https://youtube.com",
        ping: 1,
        service: SERVICE.YOUTUBE,
      } as InstanceInterface,
    },
  },
  redirection: {
    invidious: {
      isEnabled: true,
      allowedPaths: [
        {
          path: "/watch",
          parameter: "v",
        },
        {
          path: "/",
        },
      ] as AllowedPathInterface[],
    },
    piped: {
      isEnabled: true,
      allowedPaths: [
        {
          path: "/watch",
          parameter: "v",
        },
        {
          path: "/",
        },
      ] as AllowedPathInterface[],
    },
    peertube: {
      isEnabled: true,
      preferredInstance: {
        url: "",
        ping: 0,
        service: SERVICE.PEERTUBE,
      },
      allowedPaths: [
        {
          path: "/videos/watch",
        },
        {
          path: "/",
        },
      ] as AllowedPathInterface[],
    },
    youtube: {
      isEnabled: true,
      allowedPaths: [
        {
          path: "/watch",
          parameter: "v",
        },
      ] as AllowedPathInterface[],
    },
    rules: {
      matchExactTitle: {
        isEnabled: true,
        scopes: [SERVICE.INVIDIOUS, SERVICE.PIPED, SERVICE.YOUTUBE],
      },
      toPreferredPeertube: {
        isEnabled: false,
        scopes: [SERVICE.PEERTUBE],
      },
    },
  },
}
