import { Ok, Result } from "ts-results"
import Browser from "webextension-polyfill"
import * as Settings from "./lib/settings"
import * as Cache from "./lib/cache"
import { error, info } from "./lib/utils"

/**
 * @summary Clear cache & settings on extension Update
 * @returns
 */
const onUpdate = async (previousVersion: string | undefined): Promise<Result<void, Error>> => {
  switch (previousVersion) {
    case "0.1.0":
    case "0.1.1":
    case "0.1.2": {
      info("New version found. Settings and Cache will be reset")

      const result = Result.all(await Settings.reset(), await Cache.clear())

      if (result.ok) {
        return Ok.EMPTY
      } else {
        return result
      }
    }
    default:
      return Ok.EMPTY
  }
}

/**
 * @summary This function handle new installations of the extension. It includes extension updates
 * @returns
 */
export const handleInstall = (): Result<void, Error> => {
  try {
    Browser.runtime.onInstalled.addListener(async (details) => {
      switch (details.reason) {
        case "update":
          return await onUpdate(details.previousVersion)
      }
    })

    return Ok.EMPTY
  } catch (e) {
    return error(e)
  }
}
