import * as Redirection from "./lib/redirection"
import * as Settings from "./lib/settings"
import { error } from "./lib/utils"
import Browser from "webextension-polyfill"
import { BROWSER_MESSAGE } from "./lib/enums/browser-message"
import * as ExtensionUpdater from "./extension-updater"

/**
 * @summary Run background tasks such as handling redirection.
 *          This file is the entry point for background scripts defined in manifest.json
 */
const runBackgroundTasks = async () => {
  try {
    // Handle extension update
    const handleInstallResult = ExtensionUpdater.handleInstall()

    if (handleInstallResult.err) {
      throw handleInstallResult.val
    }

    // Handle Redirection
    const settingsResult = await Settings.load()

    if (settingsResult.ok) {
      const enableResult = await Redirection.enableRedirection()

      if (enableResult.ok) {
        Browser.runtime.onMessage.addListener(async (message) => {
          switch (message) {
            case BROWSER_MESSAGE.REDIRECTION_DISABLE:
            case BROWSER_MESSAGE.APP_DISABLE:
              Redirection.disableRedirection()

              break
            case BROWSER_MESSAGE.REDIRECTION_ENABLE:
            case BROWSER_MESSAGE.APP_ENABLE:
              await Redirection.enableRedirection()

              break
          }
        })
      } else {
        enableResult
      }
    } else {
      settingsResult
    }
  } catch (e: any) {
    error(e)
  }
}

runBackgroundTasks()
