# Changelog

## [v0.3.1] - 2023-08-29

### Fixed

- Fix getFirstVideoAvailable method

### Changed

- Update dependencies

## [v0.3.0] - 2023-07-09

### Changed

- Default redirector is now YouTube

## [v0.2.8] - 2023-06-17

### Fixed

- Allow to scroll on settings page on small screens
- Ignore Piped instances without frontendUrl when retrieving Piped instances
- Clearing cache now reload redirection

## [v0.2.7] - 2023-05-02

### Added

- Add command to generate sources archive

## [v0.2.6] - 2023-05-02

### Changed

- Update german translation (Thanks to vrifox!)

## [v0.2.5] - 2023-03-08

### Changed

- CSS improvements

### Fixed

- Now correctly matches special characters (`_` and `-`) in YouTube IDs

## [v0.2.4] - 2023-01-31

### Changed

- Update of the Croatian translations (Thanks to milotype!)

### Fixed

- Settings improperly loaded when config schema changed

## [v0.2.3] - 2023-01-23

### Fixed

- Now only try to redirect valid paths

## [v0.2.2] - 2023-01-23

### Changed

- Update translations

### Fixed

- Caching problem when trying to get a value that was invalidated

## [v0.2.1] - 2023-01-23

### Fixed

- Now correctly save the URL origin of preferred PeerTube instance

## [v0.2.0] - 2023-01-22

### Added

- Add Piped as Redirector
- Add option to redirect to the preferred PeerTube instance
- Reset settings and cache on extension update
- Improve css of various components
- All Invidious and Piped known instances are now redirected

### Changed

- Update german translation
- Faster ping by only requesting headers
- Update translations
- Update dependencies

### Fixed

- Use right colors for disabled dropdown
- Can no longer click twice on a button when operation is not over
- App toggle's label no longer get grayed when the app is disabled

## [v0.1.2] - 2022-07-11

### Added

- Add Crotian Translation

### Changed

- Update dependencies
  

### Removed

- Remove unecessary web-ext dependency

## [v0.1.1] - 2022-07-05

### Fixed

- Fix redirection issues when accessing an Invidious link

## [v0.1.0] - 2022-07-05

### Added

- You can now use YouTube to retrieve videos informations. It's a lot _faster_ than Invidious but YouTube can know which video you're requesting.
- You can now translate the extension using a web interface thanks to Weblate: https://weblate.framasoft.org/projects/peertube-companion/webextension/
- Fieldset legends are now localizable.
- German translation

### Changed

- Improved wording of some strings

### Removed

- Redirection settings removed from the popup. This to help non-tech people to understand the extension easier (by not overwhelming them with technical informations).

[v0.3.1]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.3.0..v0.3.1
[v0.3.0]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.2.7..v0.3.0
[v0.2.8]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.2.7..v0.2.8
[v0.2.7]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.2.6..v0.2.7
[v0.2.6]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.2.5..v0.2.6
[v0.2.5]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.2.4..v0.2.5
[v0.2.4]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.2.3..v0.2.4
[v0.2.3]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.2.2..v0.2.3
[v0.2.2]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.2.1..v0.2.2
[v0.2.1]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.2.0..v0.2.1
[v0.2.0]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.1.2..v0.2.0
[v0.1.2]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.1.1..v0.1.2
[v0.1.1]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.1.0..v0.1.1
[v0.1.0]: https://codeberg.org/Booteille/peertube-companion/releases/tag/v0.1.0
