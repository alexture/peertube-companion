# PeerTube Companion

[![État de la traduction](https://weblate.framasoft.org/widgets/peertube-companion/-/webextension/svg-badge.svg)](https://weblate.framasoft.org/projects/peertube-companion/webextension/)

## Introduction

PeerTube Companion is an experimental Web Extension aiming to improve user experience with PeerTube.

It's under heavy development and is considered unstable for now. Please, use it as your own risk.

Right now, when you try to reach a video on Youtube, the extension redirects you to a PeerTube version of the video, if there is one available.

More information about future goals will be added once I have the time.

## How does redirection work?

Redirection is handled by checking if the YT video has the same title than a video on PeerTube. If that's the case, the redirection is made.
Getting a perfect redirection because of several factors:

- With the current title-based method, a video could have a different title but be the same, or have the same title but not be the same.
- It would be difficult to base redirection on a different factor like SHA since PeerTube often reencodes the video and so the SHA of the YouTube video would always be different on PeerTube.

## How can I speed up redirection?

You can currently use two backends for redirection: Invidious and YouTube. Using YouTube is much faster due to how Invidious (and Piped) work, but sends your IP to YouTube servers.

## Download

<a href="https://addons.mozilla.org/firefox/addon/peertube-companion/"><img src="https://user-images.githubusercontent.com/585534/107280546-7b9b2a00-6a26-11eb-8f9f-f95932f4bfec.png" alt="Get PeerTube companion for Firefox"></a>
## Build from source

To build the webextension, you'll need to install yarn then use:

```console
yarn install
yarn build:prod
```

## Translate

If you want to help to translate this Web Extension, you can do so by using Weblate, an opensource platform for translating projects.
The instance we use for PeerTube Companion is hosted by [Framasoft](https://weblate.framasoft.org/engage/peertube-companion/), a french non-for-profit organization.

## Credits

This awesome Sepia illustration has been made by [David Revoy](https://www.davidrevoy.com/) for [PeerTube](https://joinpeertube.org/) project. It's licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr).
